<?php

namespace Genasyst\Webasyst\Database;


class Model extends \waModel {

   public function __construct($table = null, $type = null, $writable = false)
   {
       $this->table = $table;
       parent::__construct($type, $writable);
   }
}