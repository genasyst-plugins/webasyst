<?php

namespace Genasyst\Webasyst\Database;


class Repository {

    /**
     * @var null | \waModel
     */
    protected $model = null;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return \waModel
     */
    public function getModel()
    {
        return $this->model;
    }

}